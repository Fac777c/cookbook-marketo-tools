default['marketo-tools']['install_dir'] = '/home/gitlab-mrkt/marketo-tools'
default['marketo-tools']['unicorn_socket_path'] = File.join(node['marketo-tools']['install_dir'], 'tmp/sockets/unicorn.socket')
default['marketo-tools']['unicorn_worker_processes'] = 2

default['marketo-tools']['ssl_certificate'] = ''
default['marketo-tools']['ssl_key'] = ''
default['marketo-tools']['listen_port'] = 4567
default['marketo-tools']['env'] = [] # example: ['FOO="1"', 'BAR="2"']

default['marketo-tools']['ruby_version'] = '2.1.6'
default['marketo-tools']['ruby_build_revision'] = 'e9d25d0f615d2dc2e6b7b32562d6a4eee9e9889e' # random revision which knows how to build 2.1.6
default['marketo-tools']['rubygems_version'] = '2.4.8'
default['marketo-tools']['bundler_version'] = '1.9.9'