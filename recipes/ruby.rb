# Install Ruby from source on Ubuntu 14.04

ruby_prefix = '/usr/local'

package 'git' # Needed for everything

# Packages for compiling Ruby
%w{
g++ gcc make libc6-dev libreadline6-dev zlib1g-dev
libssl-dev libyaml-dev libsqlite3-dev sqlite3 autoconf libgdbm-dev
libncurses5-dev automake libtool bison pkg-config libffi-dev
}.each do |pkg|
  package pkg
end

ruby_build_install_dir = "/root/ruby-build"

git ruby_build_install_dir do
  repository 'https://github.com/sstephenson/ruby-build.git'
  revision node['marketo-tools']['ruby_build_revision']
  notifies :run, 'execute[install ruby-build]', :immediately
end

execute 'install ruby-build' do
  command './install.sh'
  cwd ruby_build_install_dir
  env 'PREFIX' => ruby_prefix
  action :nothing
end

ruby_version = node['marketo-tools']['ruby_version']
execute "#{ruby_prefix}/bin/ruby-build #{ruby_version} #{ruby_prefix}" do
  not_if "#{ruby_prefix}/bin/ruby --version | grep 'ruby #{ruby_version}'"
end

rubygems_version = node['marketo-tools']['rubygems_version']
execute "#{ruby_prefix}/bin/gem update --system #{rubygems_version} --no-ri --no-rdoc" do
  not_if "#{ruby_prefix}/bin/gem --version | grep '^#{rubygems_version}$'"
end

bundler_version = node['marketo-tools']['bundler_version']
execute "#{ruby_prefix}/bin/gem install bundler --version #{bundler_version} --no-ri --no-rdoc" do
  not_if "#{ruby_prefix}/bin/bundle --version | grep ' #{bundler_version}$'"
end