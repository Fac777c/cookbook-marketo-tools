marketo_tools_conf = GitLab::Vault.get(node, 'marketo-tools')

package 'nginx'

# we don't want to serve a static site at port 80
file '/etc/nginx/sites-enabled/default' do
  action :delete
end

service 'nginx' do
  supports reload: true
end

cert_path = '/etc/ssl/marketo-tools.crt'
key_path = '/etc/ssl/marketo-tools.key'

file cert_path do
  content(marketo_tools_conf['ssl_certificate'] + "\n")
  notifies :reload, 'service[nginx]'
end

file key_path do
  content(marketo_tools_conf['ssl_key'] + "\n")
  mode 0400
  notifies :reload, 'service[nginx]'
end

nginx_conf = '/etc/nginx/sites-available/marketo-tools'

template nginx_conf do
  variables(
    socket_path: marketo_tools_conf['unicorn_socket_path'],
    listen_port: marketo_tools_conf['listen_port'],
    cert_path: cert_path,
    key_path: key_path,
  )

  notifies :reload, 'service[nginx]'
end

link '/etc/nginx/sites-enabled/marketo-tools' do
  to nginx_conf
  notifies :reload, 'service[nginx]'
end

